<?php

namespace App\Traits;


use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

trait FileProcessing
{

    public static function uploadFile($dir, $file)
    {
        $path = implode('/', ['media', $dir]);
        return str_replace($path . '/', '', $file->store($path));
    }

    public static function generateImageURL($path)
    {
        $image = [];
        $fullPath = storage_path('media/' . $path);
        if (\File::exists($fullPath)) {
            $image['file'] = \File::get($fullPath);
            $image['type'] = \File::mimeType($fullPath);

            return $image;
        } else {
            throw new FileNotFoundException($fullPath);
        }
    }
}
