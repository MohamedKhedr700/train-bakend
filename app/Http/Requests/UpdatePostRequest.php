<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['sometimes', 'required', 'string', 'max:255'],
            'body' => ['sometimes', 'required', 'string', 'max:255'],
            'image' => ['sometimes', 'required', 'image', 'mimes:jpeg,jpg,png', 'max:5000'],
        ];
    }

    /**
     * Translate the request attributes
     * @return array
     */
    public function attributes(): array
    {
        return [
            'title' => trans('posts.title'),
            'body' => trans('posts.body'),
            'image' => trans('posts.image'),
        ];
    }
}
