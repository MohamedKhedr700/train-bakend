<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'string', 'unique:users,email', 'max:255'],
            'password' => ['required', 'confirmed', 'min:8', 'max:100', 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[&-_.!@#$%*?+~])/'],
        ];
    }

    /**
     * Translate the request attributes
     * @return array
     */
    public function attributes(): array
    {
        return [
            'name' => trans('users.name'),
            'email' => trans('users.email'),
            'password' => trans('users.password'),
            'password_confirmation' => trans('users.passwordConfirmation'),
        ];
    }
}
