<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class RegisterController extends Controller
{
    public $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Register User Into System.
     * @param RegisterRequest $request
     * @return JsonResponse
     */

    public function register(RegisterRequest $request): JsonResponse
    {
        $user = $this->model->create($request->only(['name', 'email', 'password']));
        return $user ? response()->json(['message' => trans('users.user') . ' ' . trans('messages.create.success'), 'data' => ['accessToken' => $user->createToken($user->id)->plainTextToken, 'userId' => $user->id]]) : response()->json(['message' => trans('users.user ') . ' ' . trans('messages.create.fail')]);
    }

}
