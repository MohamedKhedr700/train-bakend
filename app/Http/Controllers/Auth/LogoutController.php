<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class LogoutController extends Controller
{
    public $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * Logout User From System.
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        return auth()->user()->tokens()->delete() ? response()->json(['message' => trans('messages.logout.success')]) : response()->json(['message' => trans('messages.logout.fail')]);
    }
}
