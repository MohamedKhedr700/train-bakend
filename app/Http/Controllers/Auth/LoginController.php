<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    public $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }


    /**
     * Login User Into System.
     * @param LoginRequest $request
     * @return JsonResponse
     * @throws ValidationException
     */

    public function login(LoginRequest $request): JsonResponse
    {
        $data = $request->only(['email', 'password']);
        $user = $this->model->where('email', $data['email'])->first();
        if ($user && $user->validatePasswordGrant($data['password'])) {
            return response()->json(['message' => trans('auth.success'), 'data' => ['accessToken' => $user->createToken($user->id)->plainTextToken, 'userId' => $user->id]]);
        } else {
            return $this->sendFailedLoginResponse();
        }
    }

    /**
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse()
    {
        throw ValidationException::withMessages([
            'email' => [trans('auth.failed')],
        ]);
    }
}
