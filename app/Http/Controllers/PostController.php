<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexPostRequest;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use ArrayKeysCaseTransform\ArrayKeys;
use Illuminate\Http\JsonResponse;

class PostController extends Controller
{
    public $model;

    public function __construct(Post $model)
    {
        $this->model = $model;
    }

    /**
     * Store Post.
     * @param StorePostRequest $request
     * @return JsonResponse
     */
    public function store(StorePostRequest $request): JsonResponse
    {
        $data = $request->only(['title', 'body', 'image']);
        return $this->model->create(ArrayKeys::toSnakeCase($data)) ? response()->json(['message' => trans('posts.post') . ' ' . trans('messages.create.success')]) : response()->json(['message' => trans('posts.post') . ' ' . trans('messages.create.fail')]);
    }

    /**
     * Index Posts.
     * @param IndexPostRequest $request
     * @return JsonResponse
     */
    public function index(IndexPostRequest $request): JsonResponse
    {
        $data = $request->only(['perPage']) ?? 0;
        return $this->model->count() ? response()->json(PostCollection::make($this->model->paginate((int)$data['perPage']))) : response()->json(['message' => trans('posts.posts') . ' ' . trans('messages.index.areNotFound')])->setStatusCode(404);
    }

    /**
     * Show Post By ID.
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $post = $this->model->find($id);
        return !is_null($post) ? response()->json(['data' => PostResource::make($post)]) : response()->json(['message' => trans('posts.post') . ' ' . trans('messages.index.isNotFound')])->setStatusCode(404);
    }

    /**
     * Update Post By ID.
     * @param UpdatePostRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function update(UpdatePostRequest $request, $id): JsonResponse
    {
        $data = $request->only(['title', 'body', 'image']);
        $post = $this->model->find($id);
        if (!is_null($post)) {
            return $post->update(ArrayKeys::toSnakeCase($data)) ? response()->json(['message' => trans('posts.post') . ' ' . trans('messages.update.success')]) : response()->json(['message' => trans('posts.post') . ' ' . trans('messages.update.fail')]);
        } else {
            return response()->json(['message' => trans('posts.post') . ' ' . trans('messages.index.isNotFound')]);
        }
    }

    /**
     * Delete Post By ID.
     * @param $id
     * @return JsonResponse
     */
    public function delete($id): JsonResponse
    {
        $post = $this->model->find($id);
        if (!is_null($post)) {
            return $post->delete() ? response()->json(['message' => trans('posts.post') . ' ' . trans('messages.delete.success')]) : response()->json(['message' => trans('posts.post') . ' ' . trans('messages.delete.fail')]);
        } else {
            return response()->json(['message' => trans('posts.post') . ' ' . trans('messages.index.isNotFound')]);
        }
    }
}
