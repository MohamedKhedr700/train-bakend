<?php

namespace App\Models;

use App\Traits\FileProcessing;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Jenssegers\Mongodb\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'body',
        'image',
    ];


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->user_id = auth()->id();
        });
    }

    /**
     * @param UploadedFile $value
     * @return bool
     */
    public function setImageAttribute(UploadedFile $value): bool
    {
        return (bool)$this->attributes['image'] = FileProcessing::uploadFile('posts', $value);
    }

    /**
     * @return string
     */
    public function getImageAttribute(): string
    {
        return route('image.show', ['posts', $this->attributes['image']]);
    }
}
