<?php

return [
    'create' => [
        'success' => 'Created Successfully',
        'fail' => 'Failed To Create',
    ],
    'index' => [
        'isNotFound' => 'Is Not Found',
        'areNotFound' => 'Are Not Found',
    ],
    'update' => [
        'success' => 'Updated Successfully',
        'fail' => 'Failed To Update',
    ],
    'delete' => [
        'success' => 'Deleted Successfully',
        'fail' => 'Failed To Delete',
    ],
    'logout' => [
        'success' => 'Logged Out Successfully',
        'fail' => 'Failed To Logout',
    ],
];
