<?php

return [
    'user' => 'User',
    'name' => 'name',
    'email' => 'email',
    'password' => 'password',
    'passwordConfirmation' => 'password confirmation',
];
