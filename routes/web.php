<?php

use App\Traits\FileProcessing;
use Illuminate\Support\Facades\Route;
use Intervention\Image\Facades\Image;
use League\Flysystem\FileNotFoundException;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('image/{dir}/{image}', function ($dir, $image) {
    $imageFile = FileProcessing::generateImageURL($dir . '/' . $image);
    if (!$imageFile) {
        throw new FileNotFoundException();
    }

    try {
        return Image::make(Image::make($imageFile['file']))->response();
    } catch (Exception $exception) {
        return response()->download(storage_path('media/' . $dir . '/' . $image));
    }
})->name('image.show');
